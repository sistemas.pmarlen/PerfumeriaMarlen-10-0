/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.desktopapp.control;

import com.pmarlen.desktopapp.dao.LineaDAOFactory;
import com.pmarlen.desktopapp.dao.jdbc.DataSourceAdaptor;
import com.pmarlen.desktopapp.model.entity.Linea;
import java.sql.Connection;
import java.util.List;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

/**
 *
 * @author alfredo
 */
public class Main {

	private static Logger logger = Logger.getLogger(Main.class);
	
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {		
		
		isSingleInstanceRunning();
		logger.info("============== main ===============");
		final List<Linea> lineaList = LineaDAOFactory.getLineaDAO().getAll();
		for(Linea l: lineaList){
			logger.info("main:-> linea:"+l);
		}
	}
	
	private static void isSingleInstanceRunning(){
		Connection conn = null;
		try{
			conn = DataSourceAdaptor.getConnection();			
		} catch(Exception ex){
			ex.printStackTrace(System.err);
		} finally {
			if(conn == null) {
				System.err.println("-->> Another instance is running !");
				
				JOptionPane.showMessageDialog(null, "Ya esta corriendo la Aplicación en otra ventana", "Iniciar", JOptionPane.ERROR_MESSAGE);
				
				System.exit(1);
			}
		}
	}

}
