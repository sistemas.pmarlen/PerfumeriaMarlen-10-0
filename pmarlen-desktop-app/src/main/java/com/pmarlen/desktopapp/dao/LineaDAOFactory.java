/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.desktopapp.dao;

import com.pmarlen.desktopapp.dao.jdbc.DataSourceAdaptor;
import com.pmarlen.desktopapp.dao.jdbc.LineaDAOJDBC;
import java.sql.SQLException;

/**
 *
 * @author alfredo
 */
public class LineaDAOFactory {

	private static LineaDAO LineaDAO;

	public static LineaDAO getLineaDAO() {
		if (LineaDAO == null) {
			try {
				LineaDAO = new LineaDAOJDBC(DataSourceAdaptor.getConnection());
			} catch (SQLException ex) {
			}
		}
		return LineaDAO;
	}
}
