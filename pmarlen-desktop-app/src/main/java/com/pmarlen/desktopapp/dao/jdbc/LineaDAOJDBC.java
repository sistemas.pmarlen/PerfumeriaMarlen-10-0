/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.desktopapp.dao.jdbc;

import com.pmarlen.desktopapp.model.entity.Linea;
import com.pmarlen.desktopapp.dao.EntidadExistenteException;
import com.pmarlen.desktopapp.dao.EntidadInexistenteException;
import com.pmarlen.desktopapp.dao.LineaDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author alfredo
 */
public final class LineaDAOJDBC implements LineaDAO{
	private Connection conn;
	private Logger logger;

	public LineaDAOJDBC(Connection connection) {
		logger = Logger.getLogger(LineaDAOJDBC.class);
		logger.info("-->> init with connection:"+connection);
		conn = connection;
	}
	
	@Override
	public List<Linea> getAll() {
		List<Linea> r = new ArrayList<Linea>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT ID,NOMBRE FROM LINEA");
			
			rs = ps.executeQuery();
			while(rs.next()) {
				Linea x = new Linea();
				x.setNombre	(rs.getString("NOMBRE"));				
				r.add(x);
			}
		}catch(SQLException ex) {
			logger.error("in getAll:", ex);
			r = null;
		} finally {
			if(rs != null) {
				try{
					rs.close();
					ps.close();
				}catch(SQLException ex) {
				}
			}
		}
		return r;
	}

	@Override
	public Linea getLinea(String id) {
		Linea x = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT ID,NOMBRE FROM LINEA "+
					"WHERE ID=?");
			ps.setString(1, id);
			
			rs = ps.executeQuery();
			if(rs.next()) {
				x = new Linea();
				
				x.setId  	(rs.getInt   ("ID"));
				x.setNombre	(rs.getString("NOMBRE"));				
			}
		}catch(SQLException ex) {
			logger.error("in getLinea", ex);			
		} finally {
			if(rs != null) {
				try{
					rs.close();
					ps.close();
				}catch(SQLException ex) {
				}
			}
		}
		return x;
	}

	@Override
	public Linea edit(Linea Linea) throws EntidadInexistenteException {
		Linea x = null;
		PreparedStatement ps = null;
		int ra= -1;
		try {
			ps = conn.prepareStatement("UPDATE LINEA SET NOMBRE=? "+
					"WHERE ID=?");
			ps.setString (1, Linea.getNombre());
			ps.setInt    (2, Linea.getId());
			
			ra = ps.executeUpdate();
			if(ra != 1) {
				x = null;
				throw new EntidadInexistenteException();
			} else{
				x = Linea;
			}
			
		}catch(SQLException ex) {
			logger.error("in edit:", ex);
			x = null;
		} finally {
			if(ps != null) {
				try{
					ps.close();
				}catch(SQLException ex) {
				}
			}
		}
		return x;	
	}

	@Override
	public Linea persist(Linea Linea) throws EntidadExistenteException {
		Linea x = null;
		PreparedStatement ps = null;
		int ra= -1;
		try {
			ps = conn.prepareStatement("INSERT INTO LINEA (ID,NOMBRE) "+
					" VALUES(?,?)");
			
			ps.setInt   (1, Linea.getId());			
			ps.setString(2, Linea.getNombre());

			ra = ps.executeUpdate();
			if(ra != 1) {
				x = null;
				throw new EntidadExistenteException();
			} else{
				x = Linea;
			}
			
		}catch(SQLException ex) {
			logger.error("in insert:", ex);
			x = null;
		} finally {
			if(ps != null) {
				try{
					ps.close();
				}catch(SQLException ex) {
				}
			}
		}
		return x;
	}

	@Override
	public Linea delete(Linea Linea) throws EntidadInexistenteException {
		Linea x = null;
		PreparedStatement ps = null;
		int ra= -1;
		try {
			ps = conn.prepareStatement("DELETE FROM LINEA WHERE ID=?");
			ps.setInt(1, Linea.getId());
			
			ra = ps.executeUpdate();
			if(ra != 1) {
				x = null;
				throw new EntidadInexistenteException();
			} else{
				x = Linea;
			}
			
		}catch(SQLException ex) {
			logger.error("in delete:", ex);
			x = null;
		} finally {
			if(ps != null) {
				try{
					ps.close();
				}catch(SQLException ex) {
				}
			}
		}
		return x;
	}	

	/**
	 * @return the conn
	 */
	public Connection getConn() {
		return conn;
	}

	/**
	 * @param conn the conn to set
	 */
	public void setConn(Connection conn) {
		this.conn = conn;
	}
}
