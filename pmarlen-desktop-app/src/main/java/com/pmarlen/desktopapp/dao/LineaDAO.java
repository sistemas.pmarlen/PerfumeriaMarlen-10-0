/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.desktopapp.dao;

import com.pmarlen.desktopapp.model.entity.Linea;
import java.util.List;

/**
 *
 * @author alfredo
 */
public interface LineaDAO {
	List<Linea> getAll();
	Linea  getLinea(String codigo);
	Linea  edit(Linea Linea) throws EntidadInexistenteException;
	Linea  persist(Linea Linea) throws EntidadExistenteException;
	Linea  delete(Linea Linea) throws EntidadInexistenteException;
}
