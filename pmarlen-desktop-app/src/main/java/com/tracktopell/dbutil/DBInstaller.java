package com.tracktopell.dbutil;

import java.io.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;


/**
 *
 * @author Alfredo Estrada
 */
public abstract class DBInstaller {

	public static final String CREATION_SCRIPT = "creation_script";
	public static final String INIT_ENVIRONMENT_SCRIPTS = "environment_script";
	protected static final String PARAM_CONNECTION_JDBC_CLASS_DRIVER = "jdbc.driverClassName";
	public static boolean logDebug = false;
	protected Properties parameters4CreateAndExecute;
	//protected  final String JDBC_CLASS_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	//protected  final String PARAMS_PROPERTIES = "/jdbc.properties";
	//protected Logger logger;

	public DBInstaller(String proConfigLocation,String masterHost) throws IOException {
		//logger = LoggerFactory.getLogger(DBInstaller.class);

		if(logDebug) System.out.println("DBInstaller(proConfigLocation=" + proConfigLocation+", masterHost="+masterHost+")");

		InputStream propertiesIS = null;

		if (proConfigLocation.startsWith("file:")) {
			String fileToLaod = proConfigLocation.substring("file:".length());
			if(logDebug) System.out.println("DBInstaller(): fileToLaod=" + fileToLaod + ", can read ?" + new File(fileToLaod).canRead());
			propertiesIS = new FileInputStream(new File(fileToLaod));
		} else if (proConfigLocation.startsWith("classpath:")) {
			String resource = proConfigLocation.substring("classpath:".length());
			if(logDebug) System.out.println("DBInstaller(): resource=" + resource);
			propertiesIS = DBInstaller.class.getResourceAsStream(resource);
		} else {
			throw new IOException("For read properties, Pattern {\"file:\" | \"classpath:\"} not found in " + proConfigLocation);
		}

		parameters4CreateAndExecute = new Properties();

		if(logDebug) System.out.println("DBInstaller(): try to load properties from proConfigLocation from InputStream:" + propertiesIS);
		parameters4CreateAndExecute.load(propertiesIS);
		if(logDebug) System.out.println("DBInstaller(): Ok, loaded parameters4CreateAndExecute");
		
		if(masterHost != null){
			if(logDebug) System.out.println("\tDBInstaller(): ===> replaceing for masterHost="+masterHost);
			parameters4CreateAndExecute.put("jdbc.url",parameters4CreateAndExecute.getProperty("jdbc.url").replace("${master.host}", masterHost));
			parameters4CreateAndExecute.put("jdbc.url_prefix",parameters4CreateAndExecute.getProperty("jdbc.url_prefix").replace("${master.host}", masterHost));
			parameters4CreateAndExecute.put("jdbc.url_replace",parameters4CreateAndExecute.getProperty("jdbc.url_replace").replace("${master.host}", masterHost));			
		}
		
		if(logDebug) System.out.println("DBInstaller(): parameters4CreateAndExecute=" + parameters4CreateAndExecute);
	}

	protected Properties preparePropertiesForConnection() {
		Properties prop4Connection = new Properties();
		Enumeration<String> propertyNames = (Enumeration<String>) parameters4CreateAndExecute.propertyNames();

		while (propertyNames.hasMoreElements()) {
			String propName = propertyNames.nextElement();
			if (propName.startsWith("jdbc.urlconn.")) {
				prop4Connection.put(propName.substring("jdbc.urlconn.".length()), parameters4CreateAndExecute.getProperty(propName));
			}
		}

		if(logDebug) System.out.println("preparePropertiesForConnection: prop4Connection=" + prop4Connection);

		return prop4Connection;
	}

	protected abstract Properties preparePropertiesForCreateConnection();

	protected abstract Connection createConnectionForCreate() throws IllegalStateException, SQLException;

	protected Connection createConnectionForDBInstaller() throws IllegalStateException, SQLException {
		Connection conn = null;
		try {
			if(logDebug) System.out.println("createConnectionForInit: ...try get Connection for Create DB.");
			Class.forName(parameters4CreateAndExecute.getProperty(PARAM_CONNECTION_JDBC_CLASS_DRIVER)).newInstance();
		} catch (ClassNotFoundException ex) {
			throw new IllegalStateException(ex.getMessage());
		} catch (InstantiationException ex) {
			throw new IllegalStateException(ex.getMessage());
		} catch (IllegalAccessException ex) {
			throw new IllegalStateException(ex.getMessage());
		}

		if(logDebug) System.out.println("createConnectionForInit:Ok, Loaded JDBC Driver.");
		String urlConnection = parameters4CreateAndExecute.getProperty("jdbc.url");

		if (urlConnection.contains("${db.name}")) {
			urlConnection = urlConnection.replace("${db.name}", parameters4CreateAndExecute.getProperty("db.name"));
			if(logDebug) System.out.println("createConnectionForInit:replacement for variable db.name, now urlConnection=" + urlConnection);
		}

		Properties preparePropertiesForConnection = preparePropertiesForConnection();

		if(logDebug) System.out.println("createConnectionForInit:urlConnection=" + urlConnection + ", preparePropertiesForConnection()=" + preparePropertiesForConnection);
		conn = DriverManager.getConnection(urlConnection, preparePropertiesForConnection);
		if(logDebug) System.out.println("createConnectionForInit:Connected to DB.");

		printDBInfo(conn);
		return conn;
	}

	private void printDBInfo(Connection conn) throws SQLException {
		DatabaseMetaData metaData = conn.getMetaData();

		if(logDebug) System.out.println("\t=>>SchemaTerm:" + metaData.getSchemaTerm());

		if(logDebug) System.out.println("Schemas:");

		ResultSet schemas = metaData.getSchemas();
		while (schemas.next()) {
			if(logDebug) System.out.println("\t=>>" + schemas.getString("TABLE_SCHEM") + ", " + schemas.getString("TABLE_CATALOG"));
		}
		schemas.close();
		ResultSet tablesRS = metaData.getTables(null, null, "%", null);
		if(logDebug) System.out.println("Tables:");
		Statement statement = conn.createStatement();
		while (tablesRS.next()) {
			String schemaTableIter = tablesRS.getString(2);
			String tableNameIter = tablesRS.getString(3);
			if(schemaTableIter.toLowerCase().contains("sys")){
				continue;
			}
			if(logDebug) System.out.println("\t" + schemaTableIter + "." + tableNameIter+"(");
			ResultSet executeQuery = statement.executeQuery("SELECT * FROM "+schemaTableIter + "." + tableNameIter+" WHERE 1=2");
			ResultSetMetaData emptyTableMetaData = executeQuery.getMetaData();
			int columnCount = emptyTableMetaData.getColumnCount();
			for(int columNumber=1;columNumber<=columnCount;columNumber++) {
				if(columNumber==1){
					if(logDebug) System.out.print("");					
				}else{
					if(logDebug) System.out.println(",");
				}
				int columnSize   = emptyTableMetaData.getPrecision(columNumber);
				int columnDD     = emptyTableMetaData.getScale(columNumber);
				int nullableFlag = emptyTableMetaData.isNullable(columNumber);
				boolean autoIncrementFlag = emptyTableMetaData.isAutoIncrement(columNumber);				
				
				if(logDebug) System.out.print("\t\t" + emptyTableMetaData.getColumnName(columNumber)+ "  " + emptyTableMetaData.getColumnTypeName(columNumber));
				if(columnSize>0) {
					if(logDebug) System.out.print(" ( " + columnSize);
					if(columnDD>0){
					
					}
					if(logDebug) System.out.print(" )");
				}
				if(nullableFlag ==1) {
					if(logDebug) System.out.print(" NULL");
				}
				if(autoIncrementFlag) {
					if(logDebug) System.out.print(" AUTOINCREMENT");
				}
			}
			if(logDebug) System.out.println("");
			if(logDebug) System.out.println("\t);");
			executeQuery.close();
			
			
			/*
			ResultSet resColumnsTable = metaData.getColumns(null, schemaTableIter, tableNameIter, null);
			for(int columnCounter = 0;resColumnsTable.next();columnCounter++) {
				if(columnCounter>0){
					if(logDebug) System.out.println(",");
				}else{
					if(logDebug) System.out.println("");
				}
				
				
				if(logDebug) System.out.println("\t\t" + 
						resColumnsTable.getString("COLUMN_NAME")+ "  " + resColumnsTable.getString("TYPE_NAME"));
				
				int columnSize = resColumnsTable.getInt("COLUMN_SIZE");
				int columnDD   = resColumnsTable.getInt("DECIMAL_DIGITS");
				int nullableFlag = resColumnsTable.getInt("NULLABLE");
				boolean autoIncrementFlag = resColumnsTable.getString("IS_AUTOINCREMENT").equalsIgnoreCase("yes");
				
				if(columnSize>0) {
					if(logDebug) System.out.println(" ( " + columnSize);
					if(columnDD>0){
					
					}
					if(logDebug) System.out.println(" )");
				}
				if(nullableFlag ==1) {
					if(logDebug) System.out.println(" NULL");
				}
				if(autoIncrementFlag) {
					if(logDebug) System.out.println(" AUTOINCREMENT");
				}				
			}
			if(logDebug) System.out.println();
			if(logDebug) System.out.println("\t);");
			resColumnsTable.close();
			*/
		}
		tablesRS.close();

		if(logDebug) System.out.println("=======================================");
	}
	
	private void printTables(Connection conn, String tableName) throws SQLException {
		DatabaseMetaData metaData = conn.getMetaData();

		ResultSet tablesRS = metaData.getTables(null, null, "%", null);
		
		Statement statement = conn.createStatement();
		while (tablesRS.next()) {
			String schemaTableIter = tablesRS.getString(2);
			String tableNameIter = tablesRS.getString(3);
			if(schemaTableIter.toLowerCase().contains("sys")){
				continue;
			}
			if(tableName!= null && !tableName.equalsIgnoreCase(tableNameIter)){
				continue;
			}
			System.out.print(tableNameIter);
			if(tableName != null){
				
				ResultSet rsPKs = metaData.getPrimaryKeys(null, null, tableName);
				List<String> pks=new ArrayList<String>();
				while(rsPKs.next()){
					pks.add(rsPKs.getString("COLUMN_NAME").toUpperCase());
				}
				rsPKs.close();
				ResultSet rsFKs = metaData.getImportedKeys(null, null, tableName);
				HashMap<String,String> fks=new HashMap<String,String>();
				while(rsFKs.next()){
					String fkd= 
							rsFKs.getString("PKTABLE_NAME").toUpperCase()+
							"("+
							rsFKs.getString("PKCOLUMN_NAME").toUpperCase()+
							")";
					fks.put(rsFKs.getString("FKCOLUMN_NAME").toUpperCase(),
							fkd);
				}
				System.out.println("(");
				ResultSet executeQuery = statement.executeQuery("SELECT * FROM "+schemaTableIter + "." + tableNameIter+" WHERE 1=2");
				ResultSetMetaData emptyTableMetaData = executeQuery.getMetaData();
				int columnCount = emptyTableMetaData.getColumnCount();
				for(int columNumber=1;columNumber<=columnCount;columNumber++) {
					if(columNumber==1){
						System.out.print("");					
					}else{
						System.out.println(",");
					}
					int columnSize   = emptyTableMetaData.getPrecision(columNumber);
					int columnDD     = emptyTableMetaData.getScale(columNumber);
					int nullableFlag = emptyTableMetaData.isNullable(columNumber);
					boolean autoIncrementFlag = emptyTableMetaData.isAutoIncrement(columNumber);				
					final String columnName = emptyTableMetaData.getColumnName(columNumber);
					boolean primaryKeyFlag = pks.contains(columnName.toUpperCase());
					String  fkReference    = fks.get(columnName.toUpperCase());
					
					System.out.print("\t" + columnName+ "  " + emptyTableMetaData.getColumnTypeName(columNumber));
					if(columnSize>0) {
						System.out.print(" ( " + columnSize);
						if(columnDD>0){

						}
						System.out.print(" )");
					}
					if(primaryKeyFlag ) {
						System.out.print(" PRIMARY KEY");
					}
					if(nullableFlag ==1) {
						System.out.print(" NULL");
					}
					if(autoIncrementFlag) {
						System.out.print(" AUTOINCREMENT");
					}
					if(fkReference != null){
						System.out.print(" FOREIGN KEY "+fkReference);
					}
				}
				System.out.println("");
				System.out.println(")");			
				executeQuery.close();
			} else {
				System.out.println("");				
			}
			
		}
		tablesRS.close();
	}

	protected void executeScriptFrom(InputStream is, Connection conn, boolean continueWithErrors)
			throws SQLException, IOException {

		BufferedReader brInput = null;
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;
		String sql = null;

		int updateCount;
		int numberOfColumns;
		boolean prinHeaders   = true;
		
		try {
			conn.setAutoCommit(true);
			brInput = new BufferedReader(new InputStreamReader(is));

			System.out.print("sql > ");
			
			Statement sexec = conn.createStatement();

			String fullSql = "";
			while ((sql = brInput.readLine()) != null) {
				if (is != System.in) {
					System.out.println(sql);
					
				}
				if (sql.trim().toLowerCase().startsWith("exit")) {
					break;
				} 
				if (sql.trim().toLowerCase().startsWith("!")) {
					String sysCommand = sql.trim();
					
					sysCommand = sysCommand.endsWith(";")?sysCommand.replace(";", ""):sysCommand;
					
					sysCommand = sysCommand.substring(sysCommand.indexOf("!")+1);
					
					//System.err.println("sysCommand=->"+sysCommand+"<-");
					String sysCommandAndArgs[]=sysCommand.split("[ ]+");
					//System.err.println("sysCommandAndArgs="+Arrays.asList(sysCommandAndArgs));
					if(sysCommandAndArgs.length > 0){
						if(sysCommandAndArgs[0].equalsIgnoreCase("help")){
							System.err.println("!help\tPrint this commend list");
							System.err.println("!show tables\tPrint the table list for connection");
							System.err.println("!desc <table_name>\tPrint the table definition metadata");
							
						} else if(sysCommandAndArgs.length == 2 && 
								(sysCommandAndArgs[0].equalsIgnoreCase("show") && sysCommandAndArgs[1].equalsIgnoreCase("tables"))){
							printTables(conn,null);
						} else if(sysCommandAndArgs.length == 2 && 
								(sysCommandAndArgs[0].equalsIgnoreCase("desc"))){
							printTables(conn,sysCommandAndArgs[1]);
						} else {
							System.err.println("external command not found :"+sysCommand+", try !help");
						}
					
					}
					//
					System.out.println("");
					System.out.print("sql > ");
					
					continue;
				}
				if (sql.trim().length() == 0 || sql.startsWith("--")) {
					System.out.print("sql > ");
					
					continue;
				} 
				if (sql.trim().endsWith(";")) {
					fullSql += " " + sql.trim();
					try {
						fullSql = fullSql.replaceAll(";$", "");
						boolean resultExecution = false;
						if (fullSql.toLowerCase().startsWith("call ")) {
							//fullSql = fullSql.replace("call ","");
							//System.out.println("==>>conn.prepareCall("+fullSql+")");
							CallableStatement callSt = conn.prepareCall(fullSql);
							
							resultExecution = callSt.execute();
							rs = resultExecution ? callSt.getResultSet() : null;
						} else {
							resultExecution = sexec.execute(fullSql);
							rs = resultExecution ? sexec.getResultSet() : null;
						}

						if (resultExecution && rs != null) {
							
							rsmd = rs.getMetaData();
							numberOfColumns = rsmd.getColumnCount();
							if(prinHeaders){
								for (int j = 0; j < numberOfColumns; j++) {
									System.out.print((j > 0 ? "|'" : "'") + rsmd.getColumnClassName(j + 1) + "'");
								}
								System.out.println();
								for (int j = 0; j < numberOfColumns; j++) {
									System.out.print((j > 0 ? "|'" : "'") + rsmd.getColumnTypeName(j + 1) + "'");
								}
								System.out.println();
							}
							
							for (int j = 0; j < numberOfColumns; j++) {
								System.out.print((j > 0 ? "|" : "") + rsmd.getColumnLabel(j + 1));								
							}
							System.out.println();
							System.out.println("------------------------");
							
							int numRows;
							for (numRows = 0; rs.next(); numRows++) {
								for (int j = 0; j < numberOfColumns; j++) {
									Object o = rs.getObject(j + 1);
									if (o == null) {
										System.out.print((j > 0 ? "|NULL" : "NULL"));
									} else if (o.getClass().equals(String.class)) {
										System.out.print((j > 0 ? "|" : "") + "'" + rs.getString(j + 1) + "'");
									} else {
										System.out.print((j > 0 ? "|" : "") + rs.getString(j + 1));
									}
								}
								System.out.println();								
							}
							System.out.println("------------------------");
							rs.close();
							System.out.println("ROWS " + numRows);
							
						} else {
							updateCount = sexec.getUpdateCount();
							System.out.println(updateCount + " ROWS AFFECTED.");							
						}
					} catch (SQLException exExec) {
						System.out.println(exExec.getLocalizedMessage());
						if (!continueWithErrors) {
							break;
						}
					} catch (Exception exExec) {
						exExec.printStackTrace(System.out);
						if (!continueWithErrors) {
							break;
						}
					}
					fullSql = "";
					System.out.print("sql > ");					
				} else {
					fullSql += " " + sql.trim();
					System.out.print("    > ");					
				}
			}
			if(logDebug) System.out.println("SCRIPT EXECUTED..");			
		} catch (SQLException ex) {
			throw ex;
		} catch (IOException ex2) {
			throw ex2;
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (Exception ex3) {
				ex3.printStackTrace(System.out);
			}
		}
	}

	public boolean existDB() throws IllegalStateException {
		Connection connectionForCreate = null;
		try {
			connectionForCreate = createConnectionForDBInstaller();
			return true;
		} catch (SQLException ex) {
			return false;
		} finally {
			if (connectionForCreate != null) {
				try {
					connectionForCreate.close();
				} catch (SQLException ex) {
					ex.printStackTrace(System.out);
				}
			}
		}
	}
	
	public Connection getExistDB() throws IllegalStateException {
		Connection connectionForCreate = null;
		try {
			connectionForCreate = createConnectionForDBInstaller();			
		} catch (SQLException ex) {
			
		} finally {
			return connectionForCreate;
		}
	}

	public void installDBfromScratch() {
		Connection connectionForCreate = null;
		try {
			connectionForCreate = createConnectionForCreate();
			String creationScriptResource = parameters4CreateAndExecute.getProperty(CREATION_SCRIPT);

			if(logDebug) System.out.println("installDBfromScratch:->executeScriptFrom:" + creationScriptResource);

			InputStream isCreationScript = null;

			if (creationScriptResource.startsWith("classpath:")) {
				isCreationScript = DBInstaller.class.getResourceAsStream(creationScriptResource.substring("classpath:".length()));
			} else if (creationScriptResource.startsWith("file:")) {
				isCreationScript = new FileInputStream(creationScriptResource.substring("file:".length()));
			} else {
				throw new IOException("For read properties, Pattern {\"file:\" | \"classpath:\"} not found in " + creationScriptResource);
			}
			executeScriptFrom(isCreationScript, connectionForCreate, false);

			String envScripts = parameters4CreateAndExecute.getProperty(INIT_ENVIRONMENT_SCRIPTS);
			if(logDebug) System.out.println("installDBfromScratch:envScripts=" + envScripts);
			if (envScripts != null) {
				String[] scrips = envScripts.split(",");
				for (String sc : scrips) {
					if (sc.contains("/IMPORT_")) {
						String resourceToExtract = sc.substring(sc.indexOf("IMPORT_") + 7, sc.indexOf(".SQL")) + ".txt";
						String resourcePathToExtract = "/importSqlResources/" + resourceToExtract;

						if(logDebug) System.out.println("\t\tinstallDBfromScratch:->resourceToExtract=" + resourceToExtract + ", resourcePathToExtract=" + resourcePathToExtract);
						extractResource(resourceToExtract, resourcePathToExtract);
						if(logDebug) System.out.println("\t\tinstallDBfromScratch:extractResource OK !");
					}
					if(logDebug) System.out.println("\tinstallDBfromScratch:->executeScriptFrom: " + sc);
					InputStream isInitEnvScript = null;
					if (sc.startsWith("classpath:")) {
						isInitEnvScript = DBInstaller.class.getResourceAsStream(sc.substring("classpath:".length()));
					} else if (sc.startsWith("file:")) {
						isInitEnvScript = new FileInputStream(sc.substring("file:".length()));
					} else {
						throw new IOException("For read properties, Pattern {\"file:\" | \"classpath:\"} not found in " + sc);
					}
					executeScriptFrom(isInitEnvScript, connectionForCreate, false);
				}
			}
		} catch (IOException ex2) {
			ex2.printStackTrace(System.out);
			System.exit(4);
		} catch (SQLException ex1) {
			ex1.printStackTrace(System.out);
			System.exit(3);
		} finally {
			if (connectionForCreate != null) {
				try {
					connectionForCreate.close();
				} catch (SQLException ex) {
					ex.printStackTrace(System.out);
				}
			}
		}
	}
	
	public Connection getInstallDBfromScratch() {
		Connection connectionForCreate = null;
		try {
			connectionForCreate = createConnectionForCreate();
			String creationScriptResource = parameters4CreateAndExecute.getProperty(CREATION_SCRIPT);

			if(logDebug) System.out.println("installDBfromScratch:->executeScriptFrom:" + creationScriptResource);

			InputStream isCreationScript = null;

			if (creationScriptResource.startsWith("classpath:")) {
				isCreationScript = DBInstaller.class.getResourceAsStream(creationScriptResource.substring("classpath:".length()));
			} else if (creationScriptResource.startsWith("file:")) {
				isCreationScript = new FileInputStream(creationScriptResource.substring("file:".length()));
			} else {
				throw new IOException("For read properties, Pattern {\"file:\" | \"classpath:\"} not found in " + creationScriptResource);
			}
			executeScriptFrom(isCreationScript, connectionForCreate, false);

			String envScripts = parameters4CreateAndExecute.getProperty(INIT_ENVIRONMENT_SCRIPTS);
			if(logDebug) System.out.println("installDBfromScratch:envScripts=" + envScripts);
			if (envScripts != null) {
				String[] scrips = envScripts.split(",");
				for (String sc : scrips) {
					if (sc.contains("/IMPORT_")) {
						String resourceToExtract = sc.substring(sc.indexOf("IMPORT_") + 7, sc.indexOf(".SQL")) + ".txt";
						String resourcePathToExtract = "/importSqlResources/" + resourceToExtract;

						if(logDebug) System.out.println("\t\tinstallDBfromScratch:->resourceToExtract=" + resourceToExtract + ", resourcePathToExtract=" + resourcePathToExtract);
						extractResource(resourceToExtract, resourcePathToExtract);
						if(logDebug) System.out.println("\t\tinstallDBfromScratch:extractResource OK !");
					}
					if(logDebug) System.out.println("\tinstallDBfromScratch:->executeScriptFrom: " + sc);
					InputStream isInitEnvScript = null;
					if (sc.startsWith("classpath:")) {
						isInitEnvScript = DBInstaller.class.getResourceAsStream(sc.substring("classpath:".length()));
					} else if (sc.startsWith("file:")) {
						isInitEnvScript = new FileInputStream(sc.substring("file:".length()));
					} else {
						throw new IOException("For read properties, Pattern {\"file:\" | \"classpath:\"} not found in " + sc);
					}
					executeScriptFrom(isInitEnvScript, connectionForCreate, false);
				}
			}
		} catch (IOException ex2) {
			ex2.printStackTrace(System.out);
			System.exit(4);
		} catch (SQLException ex1) {
			ex1.printStackTrace(System.out);
			System.exit(3);
		} finally {
			return connectionForCreate;
		}
	}


	public void shellDB() {

		if(logDebug) System.out.println("shellDB: --------------");

		//String urlConnection = parameters4CreateAndExecute.getProperty("jdbc.url_prefix") + parameters4CreateAndExecute.getProperty("db.name");
		//if(logDebug) System.out.println("shellDB: urlConnection="+urlConnection);
		Connection connectionForInit = getExistDB();
		
		if (connectionForInit == null) {
			if(logDebug) System.out.println("shellDB:The DB does'nt exist -> installDBfromScratch !");
			connectionForInit = getInstallDBfromScratch();
		}

		try {
			if(logDebug) System.out.println("shellDB:OK, the DB exist !!");
			//connectionForInit = createConnectionForDBInstaller();
			if(logDebug) System.out.println("shellDB:OK, connected.");
			if(logDebug) System.out.println("shellDB:Ready, Now read from stdin.");
			executeScriptFrom(System.in, connectionForInit, true);
			if(logDebug) System.out.println("-> EOF stdin, end");
		} catch (IOException ex) {
			ex.printStackTrace(System.out);
			
		} catch (IllegalStateException ex) {
			ex.printStackTrace(System.out);
			
		} catch (SQLException ex) {
			ex.printStackTrace(System.out);			
		} finally {
			try {
				if (connectionForInit != null) {
					connectionForInit.close();
				}
			} catch (SQLException ex1) {
				ex1.printStackTrace(System.out);			
			}
		}
	}

	private void extractResource(String resourceToExtract, String resourcePathToExtract) throws FileNotFoundException, IOException {
		InputStream is = getClass().getResourceAsStream(resourcePathToExtract);
		OutputStream os = new FileOutputStream(resourceToExtract);
		int r;
		byte[] buffer = new byte[1024 * 128];
		while ((r = is.read(buffer, 0, buffer.length)) != -1) {
			os.write(buffer, 0, r);
		}
		is.close();
		os.close();
	}
}
