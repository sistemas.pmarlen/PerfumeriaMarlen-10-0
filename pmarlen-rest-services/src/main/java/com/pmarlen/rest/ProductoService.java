package com.pmarlen.rest;

import com.pmarlen.context.TurboContext;
import com.pmarlen.model.beans.Producto;
import com.pmarlen.model.controller.ProductoJPAController;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/producto")
public class ProductoService {
	private Logger logger;

	public ProductoService() {
		logger = LoggerFactory.getLogger(ProductoService.class);
		logger.debug("->ProductoService, created");
	}
	
	private ProductoJPAController ProductoJPAController;	
	
	@GET
	@Path("/getAll")
	@Produces("application/json")	
	public List<com.pmarlen.wscommons.services.dto.Producto> getAll() {	
		List<com.pmarlen.wscommons.services.dto.Producto> result=new ArrayList<com.pmarlen.wscommons.services.dto.Producto>();		
		if(ProductoJPAController == null) {
			ProductoJPAController = (ProductoJPAController)TurboContext.getAppContext().getBean("productoJPAController");			
		}
		final List<Producto> jpaList = ProductoJPAController.findAllEntities();
		com.pmarlen.wscommons.services.dto.Producto dto = null;
		for(Producto l: jpaList){
			dto = new com.pmarlen.wscommons.services.dto.Producto(l);			
			result.add(dto);
		}
		
		return result;
	}
	
	@GET
	@Path("/get/{productoId: [0-9]+}")
	@Produces("application/json")	
	public com.pmarlen.wscommons.services.dto.Producto get(@PathParam("productoId") String productoId) {	
		List<com.pmarlen.wscommons.services.dto.Producto> result=new ArrayList<com.pmarlen.wscommons.services.dto.Producto>();		
		if(ProductoJPAController == null) {
			ProductoJPAController = (ProductoJPAController)TurboContext.getAppContext().getBean("productoJPAController");			
		}
		Integer entityIdParsed = 0;
		entityIdParsed = Integer.parseInt(productoId);
		com.pmarlen.wscommons.services.dto.Producto dto = null;
		logger.debug("->get from ProductoJPAController.findLazyById("+entityIdParsed+")");
		Producto entityJPA = ProductoJPAController.findLazyById(entityIdParsed);
		if(entityJPA != null){
			dto = new com.pmarlen.wscommons.services.dto.Producto(entityJPA);			
		}
		return dto;
	}
}
