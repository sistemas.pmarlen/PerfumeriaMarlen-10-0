package com.pmarlen.rest;

import com.pmarlen.context.TurboContext;
import com.pmarlen.model.beans.Industria;
import com.pmarlen.model.controller.IndustriaJPAController;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/industria")
public class IndustriaService {
	private Logger logger;

	public IndustriaService() {
		logger = LoggerFactory.getLogger(IndustriaService.class);
		logger.debug("->IndustriaService, created");
	}
	
	private IndustriaJPAController IndustriaJPAController;	
	
	@GET
	@Path("/getAll")
	@Produces("application/json")	
	public List<com.pmarlen.wscommons.services.dto.Industria> getAll() {	
		List<com.pmarlen.wscommons.services.dto.Industria> result=new ArrayList<com.pmarlen.wscommons.services.dto.Industria>();		
		if(IndustriaJPAController == null) {
			IndustriaJPAController = (IndustriaJPAController)TurboContext.getAppContext().getBean("industriaJPAController");			
		}
		final List<Industria> jpaList = IndustriaJPAController.findAllEntities();
		com.pmarlen.wscommons.services.dto.Industria dto = null;
		for(Industria l: jpaList){
			dto = new com.pmarlen.wscommons.services.dto.Industria(l);			
			result.add(dto);
		}
		
		return result;
	}
}
