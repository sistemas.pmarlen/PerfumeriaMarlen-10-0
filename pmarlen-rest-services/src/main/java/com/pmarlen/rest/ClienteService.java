package com.pmarlen.rest;

import com.pmarlen.context.TurboContext;
import com.pmarlen.model.beans.Cliente;
import com.pmarlen.model.controller.ClienteJPAController;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/cliente")
public class ClienteService {
	private Logger logger;

	public ClienteService() {
		logger = LoggerFactory.getLogger(ClienteService.class);
		logger.debug("->ClienteService, created");
	}
	
	private ClienteJPAController ClienteJPAController;	
	
	@GET
	@Path("/getAll")
	@Produces("application/json")	
	public List<com.pmarlen.wscommons.services.dto.Cliente> getAll() {	
		List<com.pmarlen.wscommons.services.dto.Cliente> result=new ArrayList<com.pmarlen.wscommons.services.dto.Cliente>();		
		if(ClienteJPAController == null) {
			ClienteJPAController = (ClienteJPAController)TurboContext.getAppContext().getBean("clienteJPAController");			
		}
		final List<Cliente> jpaList = ClienteJPAController.findAllEntities();
		com.pmarlen.wscommons.services.dto.Cliente dto = null;
		for(Cliente entityJPA: jpaList){
			dto = new com.pmarlen.wscommons.services.dto.Cliente(entityJPA);			
			dto.setDireccion("x");
			result.add(dto);
		}
		
		return result;
	}
	
	@GET
	@Path("/get/{clienteId: [0-9]+}")
	@Produces("application/json")	
	public com.pmarlen.wscommons.services.dto.Cliente get(@PathParam("clienteId") String clienteId) {	
		List<com.pmarlen.wscommons.services.dto.Cliente> result=new ArrayList<com.pmarlen.wscommons.services.dto.Cliente>();		
		if(ClienteJPAController == null) {
			ClienteJPAController = (ClienteJPAController)TurboContext.getAppContext().getBean("clienteJPAController");			
		}
		Integer entityIdParsed = 0;
		entityIdParsed = Integer.parseInt(clienteId);
		com.pmarlen.wscommons.services.dto.Cliente dto = null;
		logger.debug("->get from ClienteJPAController.findLazyById("+entityIdParsed+")");
		Cliente entityJPA = ClienteJPAController.findLazyById(entityIdParsed);
		if(entityJPA != null){
			dto = new com.pmarlen.wscommons.services.dto.Cliente(entityJPA);
			dto.setDireccion("x");
		}
		return dto;
	}
}
