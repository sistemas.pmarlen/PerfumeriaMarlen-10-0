package com.pmarlen.rest;

import com.pmarlen.context.TurboContext;
import com.pmarlen.model.beans.Marca;
import com.pmarlen.model.controller.MarcaJPAController;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/marca")
public class MarcaService {
	private Logger logger;

	public MarcaService() {
		logger = LoggerFactory.getLogger(MarcaService.class);
		logger.debug("->MarcaService, created");
	}
	
	private MarcaJPAController MarcaJPAController;	
	
	@GET
	@Path("/getAll")
	@Produces("application/json")	
	public List<com.pmarlen.wscommons.services.dto.Marca> getAll() {	
		List<com.pmarlen.wscommons.services.dto.Marca> result=new ArrayList<com.pmarlen.wscommons.services.dto.Marca>();		
		if(MarcaJPAController == null) {
			MarcaJPAController = (MarcaJPAController)TurboContext.getAppContext().getBean("marcaJPAController");			
		}
		final List<Marca> jpaList = MarcaJPAController.findAllEntities();
		com.pmarlen.wscommons.services.dto.Marca dto = null;
		for(Marca l: jpaList){
			dto = new com.pmarlen.wscommons.services.dto.Marca(l);			
			result.add(dto);
		}
		
		return result;
	}
}
