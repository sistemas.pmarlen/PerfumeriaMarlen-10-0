package com.pmarlen.rest;

import com.pmarlen.context.TurboContext;
import com.pmarlen.model.beans.Linea;
import com.pmarlen.model.controller.LineaJPAController;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/linea")
public class LineaService {
	private Logger logger;

	public LineaService() {
		logger = LoggerFactory.getLogger(LineaService.class);
		logger.debug("->LineaService, created");
	}
	
	private LineaJPAController lineaJPAController;	
	
	@GET
	@Path("/getAll")
	@Produces("application/json")	
	public List<com.pmarlen.wscommons.services.dto.Linea> getAll() {	
		List<com.pmarlen.wscommons.services.dto.Linea> result=new ArrayList<com.pmarlen.wscommons.services.dto.Linea>();		
		if(lineaJPAController == null) {
			lineaJPAController = (LineaJPAController)TurboContext.getAppContext().getBean("lineaJPAController");			
		}
		final List<Linea> jpaList = lineaJPAController.findAllEntities();
		com.pmarlen.wscommons.services.dto.Linea dto = null;
		for(Linea l: jpaList){
			dto = new com.pmarlen.wscommons.services.dto.Linea(l);			
			result.add(dto);
		}
		
		return result;
	}
}
