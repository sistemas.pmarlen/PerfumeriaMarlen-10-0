/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pmarlen.context;

import org.springframework.context.ApplicationContext;

/**
 *
 * @author alfredo
 */
public class TurboContext {
	private static ApplicationContext appContext;

	public static void setAppContext(ApplicationContext appContext) {
		TurboContext.appContext = appContext;
	}

	public static ApplicationContext getAppContext() {
		return appContext;
	}	

}
